﻿Feature: ImdbApp
Create a sample IMBD (https://www.imdb.com/) application that shows a list of movies along with actors and producers who were part of it.

@addMovie
Scenario: Adding the Movie to the Imdb
	Given the movie name "Ford v Ferrari"
	And the plot "American car designer"
	And the releaseDate "5/1/2021"
	And choosen actors "1 2"
	And choosen producer "1"
	When the movie is added on Imdb
	Then the imdbApp would look like this
	| Title				 | Plot                  | ReleaseDate |
	| Ford v Ferrari	 | American car designer | 5/1/2021	   |
	And Actors are
	| MovieId | Name		   | DateOfBirth |
	| 1       | Matt Damon     | 5/1/1975    |
	| 1       | Christian Bale | 5/1/1975    |
	And Producers are
	| MovieId | Name		  | DateofBirth |
	| 1       | James Mangold | 5/1/1975    |

	
@listMovie
Scenario: List all Movies in Imdb
	Given I have movies in ImdbApp
	When i fetch the movies
	Then the imdbApp would look like this
	| Title				 | Plot                  | ReleaseDate |
	| Ford v Ferrari	 | American car designer | 5/1/2021	   |
	| Incredible Hulk	 | SuperHero Movie       | 10/2/2022   |
	| KingKong			 | MonsterVerse Movie	 | 4/6/2021	   |
	And Actors are
	| MovieId | Name           | DateOfBirth |
	| 1		  | Matt Damon     | 5/1/1975    |
	| 1		  | Christian Bale | 5/1/1975    |
	| 2		  | Edward Norton  | 5/1/1975    |
	| 2		  | Tim Roth       | 5/1/1975    |
	| 3		  | T Hiddleston   | 5/1/1975    |
	| 3		  | B Larson       | 5/1/1975    |
	And Producers are
	| MovieId | Name          | DateOfBirth |
	|1 		  | James Mangold | 5/1/1975    |
	|2		  | K Feige       | 5/1/1975    |
	|3		  | Thomas Tull   | 5/1/1975    |