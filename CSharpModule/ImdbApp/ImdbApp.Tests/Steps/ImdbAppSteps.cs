﻿using System;
using ImdbApp;
using ImdbApp.Domain;
using ImdbApp.Repository;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace ImdbApp.Tests
{
    [Binding]
    public class ImdbAppSteps
    {
        private ImdbService _imdbService;
        private string _title, _plot, _actor;
        private int  _producerNumber;
        private List<Movie> _movies;
        private readonly List<Actor> _actors = new List<Actor>();
        private readonly List<Producer> _producer = new List<Producer>();
        private readonly List<MoviePerson> _movieActor = new List<MoviePerson>();
        private readonly List<MoviePerson> _movieProducer = new List<MoviePerson>();
        private DateTime _releaseDate;


        public ImdbAppSteps()
        {
            _imdbService = new ImdbService();
        }

        [Given(@"the movie name ""(.*)""")]
        public void GivenTheMovieName(string title)
        {
            _title = title;
        }
        
        [Given(@"the plot ""(.*)""")]
        public void GivenThePlot(string plot)
        {
            _plot = plot;
        }

        [Given(@"the releaseDate ""(.*)""")]
        public void GivenTheReleaseDate(DateTime rDate)
        {
            _releaseDate = rDate;
        }

        [Given(@"choosen actors ""(.*)""")]
        public void GivenChoosenActorsAnd(string act)
        {
            _actor = act;
        }
        
        [Given(@"choosen producer ""(.*)""")]
        public void GivenChoosenProducer(int prod)
        {
            _producerNumber = prod;
        }
        
        [When(@"the movie is added on Imdb")]
        public void WhenTheMovieIsAddedOnImdb()
        {
            _imdbService.GetMovieDetails(_title, _plot, _releaseDate, _actor, _producerNumber);
        }

        [Then(@"the imdbApp would look like this")]
        public void ThenTheImdbAppWouldLookLikeThis(Table table)
        {
            _movies = _imdbService.GetMovies();
            table.CompareToSet(_movies);
        }

        [Then(@"Actors are")]
        public void ThenActorsAre(Table table)
        {
            _movies = _imdbService.GetMovies();
            int i = 1;

            foreach (var m in _movies)
            {
                //foreach (var a in m.Actors)
                //{
                //    var obj = new MoviePerson();
                //    obj.MovieId = i;
                //    obj.Name = a.Name;  
                //    obj.DateOfBirth = a.DateOfBirth;
                //    _movieActor.Add(obj);
                //}
                var obj = m.Actors.Select(o => 
                    {
                        return new MoviePerson()
                        {
                            MovieId = i,
                            Name = o.Name,
                            DateOfBirth = o.DateOfBirth,
                    }; 
                    });
                _movieActor.AddRange(obj);
                i++;
            }
            table.CompareToSet(_movieActor);
        }

        [Then(@"Producers are")]
        public void ThenProducersAre(Table table)
        {        
            _movies = _imdbService.GetMovies();
            int i = 1;
            foreach (var m in _movies)
            {
                var obj = new MoviePerson();
                obj.MovieId = i;
                obj.Name = m.Producer.Name;
                obj.DateOfBirth = m.Producer.DateOfBirth;
                _movieProducer.Add(obj);
                i++;
            }
            table.CompareToSet(_movieProducer);
        }



        [BeforeScenario("addMovie")]
        public void AddSampleMovie()
        {
            var dateString = "5/1/1975";
            DateTime date1 = DateTime.Parse(dateString, System.Globalization.CultureInfo.InvariantCulture);

            _imdbService.AddActor("Matt Damon", date1);
            _imdbService.AddActor("Christian Bale", date1);
            _imdbService.AddProducer("James Mangold", date1);

        }

        //List Movies Starts here

        [Given(@"I have movies in ImdbApp")]
        public void GivenIHaveMoviesInImdbApp()
        {
        }

        [When(@"i fetch the movies")]
        public void WhenIFetchTheMovies()
        {
            _movies = _imdbService.GetMovies();
        }

        [BeforeScenario("listMovie")]
        public void AddSampleMoviesForList()
        {

            var dateString = "5/1/1975";
            DateTime date1 = DateTime.Parse(dateString, System.Globalization.CultureInfo.InvariantCulture);
            var dateString1 = "4/6/2021";
            DateTime Kongdate = DateTime.Parse(dateString1, System.Globalization.CultureInfo.InvariantCulture);
            var dateString2 = "10/2/2022";
            DateTime Hulkdate = DateTime.Parse(dateString2, System.Globalization.CultureInfo.InvariantCulture);
            var dateString3 = "5/1/2021";
            DateTime Cardate = DateTime.Parse(dateString3, System.Globalization.CultureInfo.InvariantCulture);

            _imdbService.AddActor("B Larson", date1);
            _imdbService.AddActor("T Hiddleston", date1);
            _imdbService.AddActor("Tim Roth", date1);
            _imdbService.AddActor("Edward Norton", date1);
            _imdbService.AddActor("Christian Bale", date1);
            _imdbService.AddActor("Matt Damon", date1);
            _imdbService.AddProducer("Thomas Tull", date1);
            _imdbService.AddProducer("K Feige", date1);
            _imdbService.AddProducer("James Mangold", date1);

            _imdbService.GetMovieDetails("KingKong", "MonsterVerse Movie", Kongdate, "5 6", 3);
            _imdbService.GetMovieDetails("Incredible Hulk", "SuperHero Movie", Hulkdate, "3 4", 2);
            _imdbService.GetMovieDetails("Ford v Ferrari", "American car designer", Cardate, "1 2", 1);

        }

    }
}
