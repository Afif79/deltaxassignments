﻿using ImdbApp.Domain;
using ImdbApp.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImdbApp
{
    public class ImdbService
    {
        private readonly MovieRepository _movieRepository;
        private readonly ActorRepository _actorRepository;
        private readonly ProducerRepository _producerRepository;
        private readonly DateTime _today;

        public ImdbService()
        {
            _movieRepository = new MovieRepository();
            _actorRepository = new ActorRepository();
            _producerRepository = new ProducerRepository();
            _today = DateTime.Today;
        }

        public bool IsDateValid(DateTime releaseDate)
        {
            if ((releaseDate-_today).TotalDays >= 0)
                return false;

            return true;
        }
        public bool IsDateValidforPerson(DateTime dateOfBirth)
        {
            int Years = new DateTime(DateTime.Now.Subtract(dateOfBirth).Ticks).Year - 1;
            if (Years >= 18)
                return false;

            return true;
        }
        public void GetMovieDetails(string title, string plot, DateTime releaseDate, string choosenActors, int choosenProducer)
        {
            var ListOfActors = GetActors();
            List<Actor> selectedActors = new List<Actor>();
            int[] choosenActorsIndexes = Array.ConvertAll(choosenActors.Split(" "), int.Parse);
            for (int i = 0; i <= choosenActorsIndexes.Length - 1; i++)
                selectedActors.Add(ListOfActors[choosenActorsIndexes[i] - 1]);

            var ListofProducer = GetProducers();

            Producer selectedProducer;

            selectedProducer = ListofProducer[choosenProducer - 1];

            AddMovie(title, plot, releaseDate, selectedActors, selectedProducer);
        }
        public void AddMovie(String title, string plot, DateTime releaseDate, List<Actor> actors, Producer producer)
        {

            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(plot) || IsDateValid(releaseDate) || actors.Count < 2 || producer.Name == null)
                throw new ArgumentException(message: "Invalid Arguments");

            var movie = new Movie()
            {
                Title = title,
                Plot = plot,
                ReleaseDate = releaseDate,
                Actors = actors,
                Producer = producer
            };

            _movieRepository.Add(movie);


        }

        public void DeleteMovie(int choosedMovie)
        {
            var countOfMovies = GetMovies().Count;
            if ((choosedMovie - 1) > countOfMovies)
                throw new Exception("Please select proper movie number");
            _movieRepository.Delete(choosedMovie - 1);
        }

        public List<Movie> GetMovies()
        {
            return _movieRepository.GetMovies();
        }

        public void AddActor(String name, DateTime dateOfBirth)
        {
            if (string.IsNullOrEmpty(name) || IsDateValidforPerson(dateOfBirth))
                throw new ArgumentException("Invalid arguments");

            var actor = new Actor()
            {
                Name = name,
                DateOfBirth = dateOfBirth,
            };

            _actorRepository.Add(actor);

        }

        public List<Actor> GetActors()
        {
            return _actorRepository.Get();
        }

        public void AddProducer(String name, DateTime dateOfBirth)
        {
            if (string.IsNullOrEmpty(name) || IsDateValidforPerson(dateOfBirth))
                throw new ArgumentException("Invalid arguments");

            var producer = new Producer()
            {
                Name = name,
                DateOfBirth = dateOfBirth
            };

            _producerRepository.Add(producer);

        }
        public List<Producer> GetProducers()
        {
            return _producerRepository.Get();
        }
    }
}
