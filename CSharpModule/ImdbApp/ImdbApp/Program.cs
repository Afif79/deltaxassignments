﻿using ImdbApp.Domain;
using ImdbApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImdbApp
{
    class Program
    {
        static void Main(string[] args)
        {
            String choices = @"    1.List Movies
    2.Add Movie
    3.Add Actor
    4.Add Producer
    5.Delete Movie
    6.Exit
What do you want to do?";
            var dateString = "5/1/1975";
            DateTime date1 = DateTime.Parse(dateString, System.Globalization.CultureInfo.InvariantCulture);

            var imdbService = new ImdbService();
            List<Actor> listofActors;
            imdbService.AddActor("Matt Damon", date1);
            imdbService.AddActor("Christian Bale", date1);
            imdbService.AddProducer("James Mangold", date1);
            imdbService.AddProducer("BJ", date1);
            List<Producer> listOfProducers;

            List<Movie> listofMovies = new List<Movie>();

            int choice = 0;
            while (choice != 6)
            {
                Console.WriteLine(choices);
                choice = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("----------------------------------------------------------------");
                switch (choice)
                {
                    case 1:
                        PrintMovies();
                        break;
                    case 2:
                        AddMovie();
                        break;
                    case 3:
                        AddActor();
                        break;
                    case 4:
                        AddProducer();
                        break;
                    case 5:
                        DeleteMovie();
                        break;
                    case 6:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Enter Valid Input");
                        break;
                }
            }
            bool IsMovieListEmpty()
            {
                listofMovies = imdbService.GetMovies();
                if (listofMovies.Count == 0)
                    return true;
                return false;
            }
            void PrintMovies() {
                if(IsMovieListEmpty())
                {
                    Console.WriteLine("No Movies Here, Please Add some.");
                    Console.WriteLine("----------------------------------------------------------------");
                }
                else
                {
                    int movieNumber = 1;
                    foreach (var m in listofMovies)
                    {
                        Console.WriteLine("**********Movie Number "+movieNumber+ "**********");
                        Console.WriteLine("Title: " + m.Title);
                        Console.WriteLine("Plot: " + m.Plot);
                        Console.WriteLine("Release Date: " + m.ReleaseDate.Date.ToString("dd/MM/yyyy"));
                        Console.WriteLine("Actors: ");
                        Console.ForegroundColor = ConsoleColor.Blue;
                        foreach (var a in m.Actors)
                            Console.WriteLine(a.Name);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Producers: ");
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine(m.Producer.Name);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("----------------------------------------------------------------");
                        movieNumber++;
                    }
                }
                
            }

            void AddMovie()
            {
                Console.WriteLine("**********Enter Movie Details**********");
                Console.Write("Name: ");
                var movieName = Console.ReadLine();
                Console.Write("Year of release: ");
                var yearOfRelease = Console.ReadLine();
                DateTime date2 = DateTime.ParseExact(yearOfRelease, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                Console.Write("Plot: ");
                var plot = Console.ReadLine();
                
                listofActors = imdbService.GetActors();
                Console.Write("Choose actor: ");
                int i = 1;
                foreach (var a in listofActors)
                {
                    Console.Write(i+"."+a.Name + " | ");
                    i++;
                }
                Console.WriteLine();
                var choosenActors = Console.ReadLine();

                listOfProducers = imdbService.GetProducers();
                Console.Write("Choose producer: ");
                int j = 1;
                foreach (var p in listOfProducers)
                {
                    Console.Write(j + "." + p.Name + " | ");
                    j++;
                }
                Console.WriteLine();
                int choosenProducer = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("----------------------------------------------------------------");
                try
                {
                    imdbService.GetMovieDetails(movieName, plot, date2, choosenActors, choosenProducer);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.WriteLine("----------------------------------------------------------------");
                }
            }

            void DeleteMovie()
            {
                if(IsMovieListEmpty())
                {
                    Console.WriteLine("Add Some Movies First");
                    Console.WriteLine("----------------------------------------------------------------");
                }
                else
                {
                    PrintMovies();
                    Console.Write("Choose Movie Number to Delete: ");
                    var choosenMovie = Convert.ToInt32(Console.ReadLine());
                    imdbService.DeleteMovie(choosenMovie);
                    Console.WriteLine("Movie Number "+choosenMovie+" Deleted");
                    Console.WriteLine("----------------------------------------------------------------");
                }
            }
            void AddActor()
            {
                Console.WriteLine("**********Enter Actor Details**********");
                Console.Write("Name: ");
                var ActorName = Console.ReadLine();
                Console.Write("Date Of Birth: ");
                var DateOfBirth = Console.ReadLine();
                DateTime date3 = DateTime.ParseExact(DateOfBirth, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                Console.WriteLine("----------------------------------------------------------------");
                imdbService.AddActor(ActorName, date3);
            }

            void AddProducer()
            {
                Console.WriteLine("**********Enter Producer Details**********");
                Console.Write("Name: ");
                var ProducerName = Console.ReadLine();
                Console.Write("Date Of Birth: ");
                var DateOfBirth = Console.ReadLine();
                DateTime date4 = DateTime.ParseExact(DateOfBirth, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                Console.WriteLine("----------------------------------------------------------------");
                imdbService.AddProducer(ProducerName, date4);
            }

        }

    }
}
