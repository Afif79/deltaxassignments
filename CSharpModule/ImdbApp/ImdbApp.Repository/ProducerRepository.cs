﻿using System;
using ImdbApp.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace ImdbApp.Repository
{
    public class ProducerRepository
    {
        private readonly List<Producer> _producer;

        public ProducerRepository()
        {
            _producer = new List<Producer>();
        }

        public void Add(Producer producer)
        {
            _producer.Add(producer);
        }

        public List<Producer> Get()
        {
            return _producer.ToList();
        }

    }
}
