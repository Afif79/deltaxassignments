﻿using System;
using ImdbApp.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace ImdbApp.Repository
{
    public class ActorRepository
    {
        private readonly List<Actor> _actors;

        public ActorRepository()
        {
            _actors = new List<Actor>();
        }
        
        public void Add(Actor actor)
        {
            _actors.Add(actor);
        }

        public List<Actor> Get()
        {
            return _actors.ToList();
        }

    }
}
