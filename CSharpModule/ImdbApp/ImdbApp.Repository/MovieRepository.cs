﻿using System;
using ImdbApp.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace ImdbApp.Repository
{
    public class MovieRepository
    {
        private readonly List<Movie> _movies;

        public MovieRepository()
        {
            _movies = new List<Movie>();
        }

        public void Add(Movie movie)
        {
            _movies.Add(movie);
        }
        
        public void Delete(int id)
        {
            _movies.RemoveAt(id);
        }

        public List<Movie> GetMovies()
        {
            return _movies.ToList();
        }
    }
}
