﻿using System;
using System.Collections.Generic;
using ImdbApp;

namespace ImdbApp.Domain
{
    public class Movie
    {
        public string Title { get; set; }
        public string Plot { get; set; }
        public DateTime ReleaseDate { get; set; }

        public List<Actor> Actors = new List<Actor>();
        
        public Producer Producer;

        public Movie(string title, string plot, DateTime releaseDate, List<Actor> actors, Producer producer)
        {
            Title = title;
            Plot = plot;
            ReleaseDate = releaseDate;
            Actors = actors;
            Producer = producer;
        }

        public Movie()
        {

        }
    }
}