﻿using System;

namespace ImdbApp.Domain
{
    public class Producer : Person
    {
        public Producer(string name, DateTime dateOfBirth)
        {
            this.Name = name;
            this.DateOfBirth = dateOfBirth;
        }
        public Producer()
        {

        }

    }
}
