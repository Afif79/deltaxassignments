﻿using System;

namespace ImdbApp.Domain
{
    public class Actor : Person
    {
        public Actor(string name, DateTime dateOfBirth)
        {
            this.Name = name;
            this.DateOfBirth = dateOfBirth;
        }

        public Actor()
        {

        }
    }
}
