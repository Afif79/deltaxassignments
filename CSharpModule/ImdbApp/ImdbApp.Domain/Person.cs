﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImdbApp.Domain
{
    public class Person
    {
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }

        public Person(string name, DateTime dateOfBirth)
        {
            this.Name = name;
            this.DateOfBirth = dateOfBirth;
        }

        public Person()
        {
        }
    }
}
