﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace ImdbApp.Domain
{
    public class MoviePerson : Person
    {
        public int MovieId { get; set; }
        public MoviePerson(string name, DateTime dateOfBirth, int movieId)
        {
            this.Name = name;
            this.DateOfBirth = dateOfBirth;
            this.MovieId = movieId;
        }

        public MoviePerson()
        {

        }
    }
}
