CREATE DATABASE IMDBSession

USE IMDBSession

CREATE TABLE Actors(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name NVARCHAR(30),
	Gender NVARCHAR(10),
	DOB DATE
)

CREATE TABLE Producers(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name NVARCHAR(30),
	Company NVARCHAR(30),
	CompanyEstDate DATE
)

CREATE TABLE Movies(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name NVARCHAR(30),
	Language NVARCHAR(50),
	ProducerId INT FOREIGN KEY REFERENCES Producers(Id),
	Profit INT
)

CREATE TABLE ActorMovieMapping(
	MovieId INT,
	ActorId INT
)

INSERT INTO Actors VALUES ('Mila Kunis' ,'Female' ,'11/14/1986'),
('Robert DeNiro' ,'Male' ,'07/10/1957'),
('George Michael','Male','11/23/1978'),
('Mike Scott','Male','08/06/1969'),
('Pam Halpert','Female','09/26/1996'),
('Dame Judi Dench','Female','04/05/1947')

SELECT * FROM Actors

INSERT INTO Producers VALUES ('Arjun' ,'Fox' ,'05/14/1998'),
('Arun','Bull','09/11/2004'),
('Tom','Hanks','11/03/1987'),
('Zeshan','Male','11/14/1996'),
('Nicole','Team Coco','09/26/1992')

SELECT * FROM Producers

INSERT INTO Movies VALUES ('Rocky' ,'English' ,1,10000),
('Rocky' ,'Hindi' ,3,3000),
('Terminal' ,'English' ,4,300000),
('Rambo' ,'Hindi' ,2,93000),
('Rudy' ,'English' ,5,9600)

SELECT * FROM Movies


INSERT INTO ActorMovieMapping VALUES(1,1),(1,3),(1,5),(2,6),(2,5),
(2,4),(2,2),(3,3),(3,2),(4,1),(4,6),(4,3),(5,2),(5,5),(5,3)

SELECT * FROM ActorMovieMapping

--Update Profit of all the movies by +1000 where producer name contains 'run'
SELECT * FROM Movies

UPDATE Movies 
SET Profit = Profit+1000
WHERE ProducerId IN
(
	SELECT ProducerId 
	FROM Producers
	WHERE Name LIKE '%run%' 
)

SELECT * FROM Movies

--Find duplicate movies having the same name and their count
SELECT NAME, 
COUNT(NAME) 
FROM Movies WITH (NOLOCK)
GROUP BY NAME
HAVING COUNT(NAME) > 1

--Find the oldest actor/actress for each movie
SELECT A.Name AS [Actor's Name], A.DOB AS [Actor's DOB],  M.Name AS [Movie's Name], M.Language
FROM Actors A WITH (NOLOCK)
INNER JOIN ActorMovieMapping L ON L.ActorId = A.Id
INNER JOIN Movies M ON M.Id = L.MovieId
WHERE A.DOB IN (
	SELECT MIN(X.DOB)
	FROM Actors X WITH (NOLOCK)
	INNER JOIN ActorMovieMapping Y ON Y.ActorId = X.Id
	INNER JOIN Movies Z ON Z.Id = Y.MovieId
	WHERE Z.Id = M.Id
)

--List of producers who have not worked with actor X 'Mila Kunis'
SELECT P.Name
FROM Producers P WITH (NOLOCK)
WHERE P.Id NOT IN (
	SELECT P.Id
	FROM Producers P WITH (NOLOCK)
	INNER JOIN Movies M ON M.ProducerId = P.Id
	INNER JOIN ActorMovieMapping L ON L.MovieId = M.Id
	INNER JOIN Actors A ON A.Id = L.ActorId
	WHERE A.Name = 'Mila Kunis'
)

--List of pair of actors who have worked together in more than 2 movies
SELECT COUNT(M.MovieId) AS [Movie id], I.Name, J.Name, A.ActorId, M.ActorId 
FROM ActorMovieMapping A WITH (NOLOCK)
INNER JOIN ActorMovieMapping M ON A.MovieId = M.MovieId
INNER JOIN Actors I ON I.Id = M.ActorId
INNER JOIN Actors J ON J.Id = A.ActorId
WHERE A.ActorId > M.ActorId
GROUP BY A.ActorId, M.ActorId, I.Name, J.Name
HAVING COUNT(M.MovieId) >= 2


--Add non-clustered index on profit column of movies table
CREATE NONCLUSTERED INDEX ProfitIndex ON Movies (profit);

--Create stored procedure to return list of actors for given movie id
GO
CREATE PROCEDURE ListOfActors @MovieId INT
AS
SELECT M.ID AS [Movie Id], A.NAME AS [Actor Id]
FROM Actors A WITH (NOLOCK)
INNER JOIN ActorMovieMapping L ON L.ActorId = A.Id
INNER JOIN Movies M ON M.Id = L.MovieId
WHERE M.Id = @MovieId
GO

EXEC ListOfActors 2

--Create a function to return age for given date of birth
CREATE FUNCTION AgeCalculator(@Date DATE)
RETURNS TABLE      
AS
RETURN (
SELECT datediff(YY, @Date, getdate()) 
AS Age)

SELECT * FROM AgeCalculator('1/12/2005')


--Create a stored procedure to increase the profit (+100) of movies with given Ids (comma separated) 
GO
CREATE PROCEDURE ListOfMovies @Ids VARCHAR(10)
AS
DECLARE @SQL NVARCHAR(200)
SET @SQL = 'UPDATE Movies SET PROFIT = PROFIT+100 WHERE Id IN ( ' + @Ids + ' ) '
EXEC sp_executesql @SQL
GO

SELECT * FROM Movies WITH (NOLOCK) WHERE Id IN (1,2,3); 
EXEC ListOfMovies '1,2,3'
SELECT * FROM Movies WITH (NOLOCK) WHERE Id IN (1,2,3); 
