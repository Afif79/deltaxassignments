CREATE DATABASE School

CREATE TABLE Students(
	Name NVARCHAR(50),
	DOB DATE,
	Gender NVARCHAR(10),
	ClassId INT
)

CREATE TABLE Teachers(
	Id INT PRIMARY KEY IDENTITY(1,1), 
	Name NVARCHAR(50),
	DOB DATE,
	Gender NVARCHAR(10)
)

CREATE TABLE Classes(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name NVARCHAR(50),
	Section CHAR,
	Number INT
)
CREATE TABLE TeacherClassMapping(
	TeacherId INT,
	ClassId INT
)

INSERT INTO Classes values ('IX','A',201)
INSERT INTO Classes values ('IX','B',202)
INSERT INTO Classes values ('X','A',203)

INSERT INTO Teachers values ('Lisa Kudrow','1985/06/08','Female')
INSERT INTO Teachers values ('Monica Bing','1982/03/06','Female')
INSERT INTO Teachers values ('Chandler Bing','1978/12/17','Male')
INSERT INTO Teachers values ('Ross Geller','1993/01/26','Male')

INSERT INTO Students values ('Scotty Loman','2006/01/31','Male',1)
INSERT INTO Students values ('Adam Scott','2005/06/01','Male',1)
INSERT INTO Students values ('Natosha Beckles','2005/01/23','Female',2)
INSERT INTO Students values ('Lilly Page','2006/11/26','Female',2)
INSERT INTO Students values ('John Freeman','2006/06/14','Male',2)
INSERT INTO Students values ('Morgan Scott','2005/05/18','Male',3)
INSERT INTO Students values ('Codi Gass','2005/12/24','Female',3)
INSERT INTO Students values ('Nick Roll','2005/12/24','Male',3)
INSERT INTO Students values ('Dave Grohl','2005/02/12','Male',3)

INSERT INTO TeacherClassMapping values (1,1)
INSERT INTO TeacherClassMapping values (1,2)
INSERT INTO TeacherClassMapping values (2,2)
INSERT INTO TeacherClassMapping values (2,3)
INSERT INTO TeacherClassMapping values (3,3)
INSERT INTO TeacherClassMapping values (3,1)


SELECT * FROM Students
SELECT * FROM Teachers
SELECT * FROM Classes

--Find list of male students
SELECT Name AS [Male Students]
FROM Students
WHERE Gender='Male'

--Find list of student older than 2005/01/01
SELECT *
FROM Students
WHERE DOB <= '2005/01/01'

--Youngest student in school
SELECT *
FROM Students
WHERE DOB = (
	SELECT 
	MAX(DOB)
	FROM Students
)
--2ND APPROACH
SELECT TOP 1 *
FROM Students
ORDER BY DOB DESC

--Find student distinct birthdays
SELECT DISTINCT DOB
FROM Students

-- No of students in each class
SELECT C.NAME, COUNT(C.NAME) AS [NO OF STUDENTS]
FROM Students S
INNER JOIN Classes C
ON S.ClassId = C.Id
GROUP BY C.Name

-- No of students in each section
SELECT C.Section, COUNT(C.Section) AS [NO OF STUDENTS]
FROM Students S
INNER JOIN Classes C
ON S.ClassId = C.Id
GROUP BY C.Section

-- No of classes taught by teacher
SELECT T.Name, Count(C.Id) AS [NUMBER OF CLASSES]
FROM Teachers T
LEFT OUTER JOIN TeacherClassMapping l ON l.TeacherId = T.Id
LEFT OUTER JOIN Classes C ON C.Id = L.ClassId
GROUP BY T.Name

-- List of teachers teaching Class X
SELECT T.Name AS [Teachers Name], C.Name
FROM Teachers T
INNER JOIN TeacherClassMapping L ON L.TeacherId = T.Id
INNER JOIN Classes C ON C.Id = L.ClassId
WHERE C.Name = 'X'
GROUP BY T.NAME, C.Name

-- Classes which have more than 2 teachers teaching
SELECT C.Name AS [Class Name], C.Section, COUNT(T.Name) AS [Teachers Count]
FROM CLASSES C
INNER JOIN TeacherClassMapping M ON M.ClassId = C.Id
INNER JOIN Teachers T ON T.Id = M.TeacherId
GROUP BY C.Name, C.Section
HAVING COUNT(T.Name) > 1

-- List of students being taught by 'Lisa'
SELECT T.NAME AS [Teacher Name], S.Name AS [Student Name]
FROM Teachers T
INNER JOIN TeacherClassMapping L ON L.TeacherId = T.Id
INNER JOIN Classes C ON C.Id = L.ClassId
INNER JOIN Students S ON S.ClassId = C.Id
WHERE T.NAME LIKE 'LISA%'